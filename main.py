import logging
import os
import sys
from git import Repo
from starlette.staticfiles import StaticFiles
from starlette.responses import FileResponse
from fastapi import FastAPI, Form
from es_connector import ESConnector

log = logging.getLogger('TPDL2020 webapp')
log.setLevel(logging.INFO)

log.info('TPDL2020 Webapp (re)started')

_CURDIR = os.path.dirname(os.path.abspath(__file__))

app = FastAPI()
app.mount('/static', StaticFiles(directory=os.path.join(_CURDIR, 'static' )))

es_connector = ESConnector()


@app.get('/')
def root():
    return FileResponse(os.path.join(os.path.join(_CURDIR, 'static'),'index.html'))


# @app.get("/favicon.ico")
# def favicon():
#     return FileResponse(os.path.join(os.path.join(_CURDIR, 'static' ),'favicon.ico'))


@app.get('/api/query/')
def query_get(q='*', s:int=0, i=None):
    try:
        log.info('Executing query={q} start={s}'.format(q=q, s=s))
        result = es_connector.query_after(q, s, i)
        return result
    except Exception as e:
        log.error(e)


@app.post('/api/webhook')
def on_git_push():
    log.info('Executing webhook for Gitea')
    try:
        repo = Repo()
        repo.remotes.origin.pull()
        os.system('sudo /bin/systemctl restart paper-demo.service')
        return 200
    except:
        return 500