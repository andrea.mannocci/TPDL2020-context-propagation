# TPDL 2020 Paper 55 Companion webapp

## Setup instructions
* Clone repo and enter the root folder
* Create a virtual environment with python 3
* Install python dependencies with `pip install -r requirements.txt`
* Install Bower package manager with `npm install -g bower`
* `cd static`
* Install Bower dependencies `bower install`. It should create a folder `static/bower_components` 
* Go back to the root folder
* Run the app `uvicorn main:app --port <int> --reload` 