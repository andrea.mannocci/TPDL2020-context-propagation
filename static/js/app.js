
var app = new Vue({
  el: '#app',
  data: {
    query: null,
    hits: null,
    start: 0,
    total: null,
    index: 'propagation-before'
  },
  methods: {
    switch_index: function () {
      this.new_query()
    },
    run_query: function () {
      if (this.query == null)
        this.query = '*';
      axios
        .get('/api/query?q=' + this.query + '&s=' + this.start + '&i=' + this.index)
        .then(response => { this.hits = response.data.hits, this.total = response.data.count });
    },
    new_query: function () {
      this.start = 0;
      this.run_query();
    },
    run_example: function (n) {
      this.index = 'propagation-before'
      switch (n) {
        case 2:
          this.query = "\"60|725e95aad103035a194402a606c5826d\"";
          this.run_query();
          break;
        case 3:
          this.query = "\"PRIMAP-hist Socio-Eco dataset\"";
          this.run_query();
          break;
        case 1:
          this.query = "shc014";
          this.run_query();
          break;
      }
    },
    previous_page: function () {
      this.start = this.start - 10;
      this.run_query();
    },
    next_page: function () {
      this.start = this.start + 10;
      this.run_query();
    }


  },
  mounted() {

  }
})